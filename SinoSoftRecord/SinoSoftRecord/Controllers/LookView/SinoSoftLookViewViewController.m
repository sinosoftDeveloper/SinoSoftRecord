//
//  SinoSoftLookViewViewController.m
//  SinoSoftRecord
//
//  Created by Apple on 2017/8/31.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SinoSoftLookViewViewController.h"

@interface SinoSoftLookViewViewController ()

@end

@implementation SinoSoftLookViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationItem.title = @"查看";
    self.title = @"查看";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
