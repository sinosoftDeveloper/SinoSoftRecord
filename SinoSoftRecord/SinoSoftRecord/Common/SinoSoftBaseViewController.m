//
//  SinoSoftBaseViewController.m
//  SinoSoftRecord
//
//  Created by Apple on 2017/8/31.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SinoSoftBaseViewController.h"
#import "Constants.h"

@interface SinoSoftBaseViewController ()

@end

@implementation SinoSoftBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
   
    self.navigationController.navigationBar.backgroundColor = Color_NaviTitle;
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:0];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
