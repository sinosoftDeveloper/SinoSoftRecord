//
//  Constants.h
//  SinoSoftRecord
//
//  Created by Apple on 2017/8/31.
//  Copyright © 2017年 Apple. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define YKRGBA(r,g,b,a) [UIColor colorWithRed:r / 255.0 green:g / 255.0 blue:b / 255.0 alpha:a]
#define SINARGB(r,g,b) YKRGBA(r,g,b,1.0)
#define YKRGBHEX(h) [UIColor colorWithRed:((float)((h & 0xFF0000) >> 16))/255.0 green:((float)((h & 0xFF00) >> 8))/255.0 blue:((float)(h & 0xFF))/255.0 alpha:1.0]
#define YKRGBAHEX(h,a) [UIColor colorWithRed:((float)((h & 0xFF0000) >> 16))/255.0 green:((float)((h & 0xFF00) >> 8))/255.0 blue:((float)(h & 0xFF))/255.0 alpha:a]

#define Color_NaviTitle SINARGB(0, 127, 255)


#endif /* Constants_h */
