//
//  NSLayoutConstraint+GKLayout.m
//  AutoLayout
//
//  Created by wqc on 15/8/7.
//  Copyright (c) 2015年 wqc. All rights reserved.
//

#import "NSLayoutConstraint+YKLayout.h"
#import "UIView+AutoLayout.h"


@implementation NSLayoutConstraint (GKLayout)

-(void)autoInstall{
    
#ifdef DEBUG
    NSAssert(self.firstItem || self.secondItem, @"self.firstItem || self.secondItem");
#endif
    if (!self.firstItem && !self.secondItem) {
        NSLog(@"self.firstItem , self.secondItem cannot both be nil");
        return;
    }
    
    if ([self respondsToSelector:@selector(setActive:)]) {
        self.active = YES;
        return;
    }
    
    if (self.firstItem) {
        if (self.secondItem) {
            UIView *commonSuperview = [self.firstItem commonSuperviewWithView:self.secondItem];
            [commonSuperview addConstraint:self];
        } else {
            [self.firstItem addConstraint:self];
        }
    } else {
        [self.secondItem addConstraint:self];
    }
}

-(void)autoRemove {
    if ([self respondsToSelector:@selector(setActive:)]) {
        self.active = NO;
        return;
    }
    
    if (self.secondItem) {
        UIView* commonSuperview = [self.firstItem commonSuperviewWithView:self.secondItem];
        while (commonSuperview) {
            if ([commonSuperview.constraints containsObject:self]) {
                [commonSuperview removeConstraint:self];
                return;
            }
            commonSuperview = commonSuperview.superview;
        }
    } else {
        [self.firstItem removeConstraint:self];
        return;
    }
    NSLog(@"cannot to remove constraint");
}

@end
