//
//  UIView+AutoLayout.h
//  AutoLayout
//
//  Created by wqc on 15/5/18.
//  Copyright (c) 2015年 wqc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DimensionOrientation){
    DimensionHorizontal = NSLayoutAttributeWidth,
    DimensionVertical = NSLayoutAttributeHeight
};


@interface UIView(YKExpand)


-(UIView*)commonSuperviewWithView:(UIView*)otherView;
-(NSLayoutConstraint*)autoConstraintAttribut:(NSLayoutAttribute)attribute  toAttribute:(NSLayoutAttribute)toAttribute toView:(UIView*)toView Offset:(CGFloat)offset relation:(NSLayoutRelation)relation;

-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView;
-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withOffset:(CGFloat)offset;
-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withMultiplier:(CGFloat)multiplier;
-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withOffset:(CGFloat)offset relation:(NSLayoutRelation)relation;
-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withMultiplier:(CGFloat)multiplier relation:(NSLayoutRelation)relation;
-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withOffset:(CGFloat)offset withMultiplier:(CGFloat)multiplier relation:(NSLayoutRelation)relation;

-(NSLayoutConstraint*)autoSetDimensionToView:(UIView*)view dimension:(DimensionOrientation)dimension;
-(NSArray*)autoSetDimensionToSize:(CGSize)size;
-(NSLayoutConstraint*)autoSetDimension:(DimensionOrientation)dimension toSize:(CGFloat)size;
-(NSLayoutConstraint*)autoSetDimension:(DimensionOrientation)dimension toSize:(CGFloat)size relation:(NSLayoutRelation)relation;
-(NSLayoutConstraint*)autoSetDimension:(DimensionOrientation)dimension toView:(UIView*)view multiplier:(CGFloat)multiplier;
-(NSLayoutConstraint*)autoSetDimension:(DimensionOrientation)dimension toView:(UIView*)view multiplier:(CGFloat)multiplier offset:(CGFloat)offset;


-(NSLayoutConstraint*)autoPinToSuperviewEdge:(NSLayoutAttribute)edge withOffset:(CGFloat)offset;
-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge ofView:(UIView*)otherView;

-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge toEdge:(NSLayoutAttribute)toEdge ofView:(UIView*)otherView;
-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge ofView:(UIView*)otherView withOffset:(CGFloat)offset;
-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge toEdge:(NSLayoutAttribute)toEdge ofView:(UIView*)otherView withOffet:(CGFloat)offset;
-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge toEdge:(NSLayoutAttribute)toEdge ofView:(UIView*)otherView withOffet:(CGFloat)offset relation:(NSLayoutRelation)relation;

-(NSLayoutConstraint*)autoAlignToSuperview:(NSLayoutAttribute)axis;
-(NSLayoutConstraint*)autoAlignToSuperview:(NSLayoutAttribute)axis offset:(CGFloat)offset;
-(NSLayoutConstraint*)autoAlignView:(UIView*)view axis:(NSLayoutAttribute)axis offset:(CGFloat)offset;
-(NSArray*)autoCenterToSuperview;

@end
