//
//  NSLayoutConstraint+GKLayout.h
//  AutoLayout
//
//  Created by wqc on 15/8/7.
//  Copyright (c) 2015年 wqc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (GKLayout)
-(void)autoInstall;
-(void)autoRemove;
@end