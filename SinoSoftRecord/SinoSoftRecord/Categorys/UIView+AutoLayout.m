//
//  UIView+AutoLayout.m
//  AutoLayout
//
//  Created by wqc on 15/5/18.
//  Copyright (c) 2015年 wqc. All rights reserved.
//

#import "UIView+AutoLayout.h"
#import "NSLayoutConstraint+YKLayout.h"

@implementation UIView (YKExpand)



-(NSLayoutConstraint*)autoConstraintAttribut:(NSLayoutAttribute)attribute  toAttribute:(NSLayoutAttribute)toAttribute toView:(UIView*)toView Offset:(CGFloat)offset relation:(NSLayoutRelation)relation{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self attribute:attribute relatedBy:relation toItem:toView attribute:toAttribute multiplier:1.0 constant:offset];
    
    [constraint autoInstall];

    return constraint;
}
#pragma mark - Match Dimension
-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView {
    return [self autoMatchDimension:dimension toDimension:toDimension ofView:otherView withOffset:0];
}

-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withOffset:(CGFloat)offset {
    return [self autoMatchDimension:dimension toDimension:toDimension ofView:otherView withOffset:offset relation:NSLayoutRelationEqual];
}

-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withMultiplier:(CGFloat)multiplier {
    return [self autoMatchDimension:dimension toDimension:toDimension ofView:otherView withMultiplier:multiplier relation:NSLayoutRelationEqual];
}


-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withOffset:(CGFloat)offset relation:(NSLayoutRelation)relation {
    
    return [self autoConstraintAttribut:(NSLayoutAttribute)dimension toAttribute:(NSLayoutAttribute)toDimension toView:otherView Offset:offset relation:relation];
}

-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withMultiplier:(CGFloat)multiplier relation:(NSLayoutRelation)relation {
    
    return [self autoConstraintAttribut:(NSLayoutAttribute)dimension toAttribute:(NSLayoutAttribute)toDimension toView:otherView Offset:0.0 relation:relation];
}

-(NSLayoutConstraint*)autoMatchDimension:(DimensionOrientation)dimension toDimension:(DimensionOrientation)toDimension ofView:(UIView*)otherView withOffset:(CGFloat)offset withMultiplier:(CGFloat)multiplier relation:(NSLayoutRelation)relation {
    
    return [self autoConstraintAttribut:(NSLayoutAttribute)dimension toAttribute:(NSLayoutAttribute)toDimension toView:otherView Offset:offset relation:relation];
}

#pragma mark - Set Dimensions

-(NSArray*)autoSetDimensionToSize:(CGSize)size{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    [arr addObject:[self autoSetDimension:DimensionHorizontal toSize:size.width]];
    [arr addObject:[self autoSetDimension:DimensionVertical toSize:size.height]];
    return arr;
}

-(NSLayoutConstraint*)autoSetDimensionToView:(UIView*)view dimension:(DimensionOrientation)dimension{
    return [self autoConstraintAttribut:(NSLayoutAttribute)dimension toAttribute:(NSLayoutAttribute)dimension toView:view Offset:0 relation:NSLayoutRelationEqual];
}

-(NSLayoutConstraint*)autoSetDimension:(DimensionOrientation)dimension toSize:(CGFloat)size {
    return [self autoSetDimension:dimension toSize:size relation:NSLayoutRelationEqual];
}

-(NSLayoutConstraint*)autoSetDimension:(DimensionOrientation)dimension toView:(UIView*)view multiplier:(CGFloat)multiplier {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self attribute:(NSLayoutAttribute)dimension relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:multiplier constant:0];
    [constraint autoInstall];
    return constraint;
}

-(NSLayoutConstraint*)autoSetDimension:(DimensionOrientation)dimension toView:(UIView*)view multiplier:(CGFloat)multiplier offset:(CGFloat)offset {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self attribute:(NSLayoutAttribute)dimension relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeWidth multiplier:multiplier constant:offset];
    [constraint autoInstall];
    return constraint;
}

-(NSLayoutConstraint*)autoSetDimension:(DimensionOrientation)dimension toSize:(CGFloat)size relation:(NSLayoutRelation)relation {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self attribute:(NSLayoutAttribute)dimension relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0.0 constant:size];
    [constraint autoInstall];
    return constraint;
}


#pragma mark - Pin Edge

-(NSLayoutConstraint*)autoPinToSuperviewEdge:(NSLayoutAttribute)edge withOffset:(CGFloat)offset {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    UIView *superview = self.superview;
#if DEBUG
    NSAssert(superview, @"super view must not nil");
#else
    if(!superview)
        return nil;
#endif
    
    if (edge == NSLayoutAttributeBottom || edge == NSLayoutAttributeTrailing || edge == NSLayoutAttributeRight) {
        offset = -offset;
    }
    
    return [self autoPinEdge:edge toEdge:edge ofView:superview withOffet:offset];
}

-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge ofView:(UIView*)otherView {
    return [self autoConstraintAttribut:edge toAttribute:edge toView:otherView Offset:0.0 relation:NSLayoutRelationEqual];
}

-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge ofView:(UIView*)otherView withOffset:(CGFloat)offset {
    return [self autoConstraintAttribut:edge toAttribute:edge toView:otherView Offset:offset relation:NSLayoutRelationEqual];
}

-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge toEdge:(NSLayoutAttribute)toEdge ofView:(UIView*)otherView {
    return [self autoConstraintAttribut:edge toAttribute:toEdge toView:otherView Offset:0.0 relation:NSLayoutRelationEqual];
}

-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge toEdge:(NSLayoutAttribute)toEdge ofView:(UIView*)otherView withOffet:(CGFloat)offset {
    return [self autoConstraintAttribut:edge toAttribute:toEdge toView:otherView Offset:offset relation:NSLayoutRelationEqual];
}

-(NSLayoutConstraint*)autoPinEdge:(NSLayoutAttribute)edge toEdge:(NSLayoutAttribute)toEdge ofView:(UIView*)otherView withOffet:(CGFloat)offset relation:(NSLayoutRelation)relation {
    return [self autoConstraintAttribut:edge toAttribute:toEdge toView:otherView Offset:offset relation:relation];
}


-(UIView*)commonSuperviewWithView:(UIView*)otherView{
    UIView *superView =nil;
    UIView *startView = self;
    do {
        if ([otherView isDescendantOfView:startView]) {
            superView = startView;
        }
        startView = startView.superview;
    } while (superView==nil && startView);
    return superView;
}


#pragma mark - Alignment

-(NSLayoutConstraint*)autoAlignToSuperview:(NSLayoutAttribute)axis {
    UIView *superview = [self superview];
#if DEBUG
    NSAssert(superview, @"super view must not nil");
#else
    if(!superview)
        return nil;
#endif
    return [self autoConstraintAttribut:axis toAttribute:axis toView:superview Offset:0 relation:NSLayoutRelationEqual];
}

-(NSLayoutConstraint*)autoAlignToSuperview:(NSLayoutAttribute)axis offset:(CGFloat)offset{
    UIView *superview = [self superview];
#if DEBUG
    NSAssert(superview, @"super view must not nil");
#else
    if(!superview)
        return nil;
#endif
    return [self autoConstraintAttribut:axis toAttribute:axis toView:superview Offset:offset relation:NSLayoutRelationEqual];
}

-(NSLayoutConstraint*)autoAlignView:(UIView*)view axis:(NSLayoutAttribute)axis offset:(CGFloat)offset{
#if DEBUG
    NSAssert(view, @"super view must not nil");
#else
    if(!view)
        return nil;
#endif
    return [self autoConstraintAttribut:axis toAttribute:axis toView:view Offset:offset relation:NSLayoutRelationEqual];
}

-(NSArray*)autoCenterToSuperview {
    UIView *superview = [self superview];
#if DEBUG
    NSAssert(superview, @"super view must not nil");
#else
    if(!superview)
        return nil;
#endif
    NSLayoutConstraint* cons1 = [self autoConstraintAttribut:NSLayoutAttributeCenterX toAttribute:NSLayoutAttributeCenterX toView:superview Offset:0 relation:NSLayoutRelationEqual];
    NSLayoutConstraint* cons2 = [self autoConstraintAttribut:NSLayoutAttributeCenterY toAttribute:NSLayoutAttributeCenterY toView:superview Offset:0 relation:NSLayoutRelationEqual];
    
    return @[cons1,cons2];
}

@end
