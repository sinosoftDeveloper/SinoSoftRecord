//
//  SinoSoftTabBarViewController.m
//  SinoSoftRecord
//
//  Created by Apple on 2017/8/31.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SinoSoftTabBarViewController.h"
#import "SinoSoftHomeViewController.h"
#import "SinoSoftLookViewViewController.h"
#import "SinoSoftMessageViewController.h"
#import "SinoSoftMyViewController.h"
#import "SinoSoftGuideViewController.h"
#import "Constants.h"

@interface SinoSoftTabBarViewController ()

@end

@implementation SinoSoftTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    SinoSoftHomeViewController *homeVC = [[SinoSoftHomeViewController alloc] init];
    homeVC.tabBarItem.title = @"首页";
    homeVC.tabBarItem.image = [UIImage imageNamed:@"shouyes.png"];
    UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeVC];
    
    SinoSoftLookViewViewController *lookVC = [[SinoSoftLookViewViewController alloc] init];
    lookVC.tabBarItem.title = @"查看";
    lookVC.tabBarItem.image = [UIImage imageNamed:@"chakans.png"];
    UINavigationController *navLook = [[UINavigationController alloc] initWithRootViewController:lookVC];
    
    SinoSoftMessageViewController *messageVC = [[SinoSoftMessageViewController alloc] init];
    messageVC.tabBarItem.title = @"消息";
    messageVC.tabBarItem.image = [UIImage imageNamed:@"xiaoxis"];
    UINavigationController *navMessage = [[UINavigationController alloc] initWithRootViewController:messageVC];
    
    
    SinoSoftGuideViewController *guideVC = [[SinoSoftGuideViewController alloc] init];
    guideVC.tabBarItem.title = @"指南";
    guideVC.tabBarItem.image = [UIImage imageNamed:@"zhinans.png"];
    UINavigationController *navGuide = [[UINavigationController alloc] initWithRootViewController:guideVC];
    
    SinoSoftMyViewController *myVC = [[SinoSoftMyViewController alloc] init];
    myVC.tabBarItem.title = @"我";
    myVC.tabBarItem.image = [UIImage imageNamed:@"wos.png"];
    UINavigationController *naviMy = [[UINavigationController alloc] initWithRootViewController:myVC];
    

    self.viewControllers = @[navHome,navLook,navMessage,navGuide,naviMy];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
